@extends('layouts.app')
@section('css')
    {{-- Datatables --}}
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    {{-- Date Range --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="container-full">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="page-title">Data {{ $pageTitle }}</h3>
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a>
                                    </li>
                                    <li class="breadcrumb-item" aria-current="page">Tabel</li>
                                    <li class="breadcrumb-item active" aria-current="page">{{ $pageTitle }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                @if (session('success'))
                    <center><span class="badge badge-success w-100"
                            style="padding: 4px; margin: 10px;">{{ session('success') }}</span></center>
                @endif
                @if (session('error'))
                    <center><span class="badge badge-danger w-100"
                            style="padding: 4px; margin: 10px;">{{ session('error') }}</span></center>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <h5 style="text-align: right">Tanggal Hari Ini: {{ date_humans(date('d-m-Y')) }}</h5>
                            </div>
                            <div class="row">
                                <h6>-Group Piket-</h6>
                                <table style="width:15%; margin-left:1%">
                                    @foreach ($master_piket as $item => $value)
                                        <tr>
                                            <th>{{ $value }}</th>
                                            <td>:</td>
                                            <td>{{ $item }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <br />
                            
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <button class="btn btn-success btn-lg btn-block">Data Absen Hari Ini, Telah di SUBMIT!!!</button>
                <!-- /.row -->
            </section>
            <!-- /.content -->

        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('js')
    <!-- Page Content overlay -->

    <!-- Vendor JS -->
    <script src="{{ asset('v1/js/vendors.min.js') }}"></script>
    <script src="{{ asset('v1/icons/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('v1/vendor_components/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" defer></script>


    <!-- EduAdmin App -->
    <script src="{{ asset('v1/js/template.js') }}"></script>

@endsection
