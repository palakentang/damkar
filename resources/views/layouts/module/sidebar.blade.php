<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">Master </li>
            <li class="treeview">
                <a href="#">
                    <i class="icon-Brush"><span class="path1"></span><span class="path2"></span></i>
                    <span>Master Data</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    {{-- <li><a href="{{ route('admin.master.mapel.index') }}"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Mata Pelajaran</a></li> --}}
                </ul>
            </li>
            
        </ul>
    </section>
    <div class="sidebar-footer">
        <!-- item-->
        <a href="javascript:void(0)" class="link" data-toggle="tooltip" title=""
            data-original-title="Settings" aria-describedby="tooltip92529"><span class="icon-Settings-2"></span></a>
        <!-- item-->
        <a href="mailbox.html" class="link" data-toggle="tooltip" title=""
            data-original-title="Email"><span class="icon-Mail"></span></a>
        <!-- item-->

    </div>
</aside>
