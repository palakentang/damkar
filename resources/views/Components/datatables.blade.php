
{!! Form::open(['route' => ['user.absen.store']]) !!}
<table id="example" class="display" style="width:100%">
    <thead>
        <tr>
            @foreach ($thead as $item)
                <td>{{$item}}</td>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach ($data['anggotas'] as $item)
            <tr>
                <td>
                    {{$item->nama_anggota}}
                </td>
                <td>{{$item->jabatan}}</td>
                <td>
                    {!! Form::select('group_piket['.$item->id.']', $data['master_piket'], $item->group_piket, ['class' => 'form-control', 'onChange' => 'pilihGroupPiket(this, '.$item->id.')']) !!}
                </td>
                <td>
                    {!! Form::text('keterangan['.$item->id.']', '', ['class' => 'form-control keterangan', 'id' => 'keterangan_'.$item->id]) !!}
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="container  pull-right">
    <div class="pull-right">
        {!! Form::submit('Submit', ['class' => 'btn btn-success']) !!}
        <Button class="btn btn-danger">Cancel</Button>
    </div>
</div>
{!! Form::close() !!}