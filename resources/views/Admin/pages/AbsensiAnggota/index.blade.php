@extends('layouts.app')
@section('css')
    {{-- Datatables --}}
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    {{-- Date Range --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="container-full">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="page-title">Data {{ $pageTitle }}</h3>
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a>
                                    </li>
                                    <li class="breadcrumb-item" aria-current="page">Tabel</li>
                                    <li class="breadcrumb-item active" aria-current="page">{{ $pageTitle }}</li>
                                </ol>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Main content -->
            <section class="content">
                @if (session('success'))
                    <center><span class="badge badge-success w-100"
                            style="padding: 4px; margin: 10px;">{{ session('success') }}</span></center>
                @endif
                @if (session('error'))
                    <center><span class="badge badge-danger w-100"
                            style="padding: 4px; margin: 10px;">{{ session('error') }}</span></center>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="container">
                            <div class="row">
                                <h5 style="text-align: right">Tanggal Hari Ini: {{ date_humans(date('d-m-Y')) }}</h5>
                            </div>
                            <div class="row">
                                <h6>-Group Piket-</h6>
                                <table style="width:15%; margin-left:1%">
                                    @foreach ($master_piket as $item => $value)
                                        <tr>
                                            <th>{{ $value }}</th>
                                            <td>:</td>
                                            <td>{{ $item }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <br />
                            <div class="row justify-content-center" id="counting_kehadiran">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div style="margin: 20px 0px;">
                                                    <strong>Date Filter:</strong>
                                                    <input type="text" name="daterange" value="" />
                                                    <input type="text" name="master_piket" value="" />
                                                    <button class="btn btn-success filter">Filter</button>
                                                </div>
                                                <div class="col-md-6">
                                                    <table style="width:100%; margin-left:1%">
                                                        <tr>
                                                            <th style="width: 30%">Total Piket Hadir</th>
                                                            <td style="width: 2%">:</td>
                                                            <td id="total_piket_hadir"
                                                                onclick="drawDatatable('{{ array_keys($master_piket)[0] }}', this)"
                                                                data-master="{{ array_keys($master_piket)[0] }}">-</td>
                                                        </tr>
                                                        <tr>
                                                            <th style="width: 30%">Total Cadangan Piket</th>
                                                            <td style="width: 2%">:</td>
                                                            <td id="total_cadangan_piket"
                                                                onclick="drawDatatable('{{ array_keys($master_piket)[1] }}', this)"
                                                                data-master="{{ array_keys($master_piket)[1] }}">-</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-md-6">
                                                    <table style="width:100%; margin-left:1%">
                                                        <tr>
                                                            <th style="width: 30%">Total Lepas Piket</th>
                                                            <td style="width: 2%">:</td>
                                                            <td id="total_lepas_piket"
                                                                onclick="drawDatatable('{{ array_keys($master_piket)[2] }}', this)"
                                                                data-master="{{ array_keys($master_piket)[2] }}">-</td>
                                                        </tr>
                                                        <tr>
                                                            <th style="width: 30%">Total Izin/Sakit/Dll</th>
                                                            <td style="width: 2%">:</td>
                                                            <td id="total_tidak_hadir"
                                                                onclick="drawDatatable('{{ array_keys($master_piket)[3] }}', this)"
                                                                data-master="{{ array_keys($master_piket)[3] }}">-</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-center" id="table_presensi">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">Daftar User</div>
                                        <div class="card-body">
                                            <table id="tbl_list" class="table table-striped table-bordered" cellspacing="0"
                                                width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Anggota</th>
                                                        <th>Status Piket</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->

        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('js')
    <!-- Page Content overlay -->

    <!-- Vendor JS -->
    <script src="{{ asset('v1/js/vendors.min.js') }}"></script>
    <script src="{{ asset('v1/icons/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('v1/vendor_components/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" defer></script>


    <!-- EduAdmin App -->
    <script src="{{ asset('v1/js/template.js') }}"></script>

    <script src="{{ asset('v1/js/pages/data-table.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#table_presensi').hide()
            $('input[name="daterange"]').daterangepicker({
                startDate: moment().subtract(1, 'M'),
                endDate: moment(),
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,  
                maxYear: parseInt(moment().format('YYYY'), 10)
            });
            var table = $('#tbl_list').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url()->current() }}',
                    data: function(d) {
                        d.from_date = $('input[name="daterange"]').data('daterangepicker').startDate
                            .format('YYYY-MM-DD');
                        d.master_piket = $('input[name="master_piket"]').val()
                    }
                },
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'anggota.nama_anggota',
                        name: 'anggota_id'
                    },
                    {
                        data: 'group_piket',
                        name: 'group_piket'
                    },
                    {
                        data: 'keterangan',
                        name: 'keterangan'
                    },

                ]
            });
            $(".filter").click(function() {
                table.draw();
                counting_present()
                $('#table_presensi').show()
            });

            function counting_present() {
                var url = '{{ route('admin.absensi.show', ':id') }}';
                url = url.replace(':id', $('input[name="daterange"]').data('daterangepicker').startDate.format(
                    'YYYY-MM-DD'));
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: "JSON",
                    success: function(response) {
                        $('#total_piket_hadir').html(response.piket_hadir)
                        $('#total_cadangan_piket').html(response.cadangan_piket)
                        $('#total_tidak_hadir').html(response.tidak_hadir)
                        $('#total_lepas_piket').html(response.lepas_tiket)
                    }
                });
            }

            

        });
        function drawDatatable(category, props) {
                if (props.innerText === '-' || props.innerText === '0') {
                    return alert('Data Tidak Ditemukan')
                }
                $('input[name=master_piket]').val(category)


            }
    </script>
@endsection
