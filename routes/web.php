<?php

use App\Http\Controllers\Admin\AbsensiAnggotaController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('auth.login.index');
});


Route::namespace('Auth')->name('auth.')->prefix('auth/')->group(function ()
{
    Route::name('login.')->prefix('login/')->group(function ()
    {
        Route::get('index',[LoginController::class, 'index'])->name('index');
        Route::post('process',[LoginController::class, 'store'])->name('store');
    });
    
    Route::name('register.')->prefix('register/')->group(function ()
    {
        Route::get('/index', [RegisterController::class, 'index'])->name('index');
        Route::get('/proccess', [RegisterController::class, 'store'])->name('store');
    });

    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
});

/**
 * Admin Area
 */
Route::group(['middleware' => ['role:Admin', 'auth']], function () {
    
    Route::name('admin.')->prefix('/admin')->group(function ()
    {
        Route::resource('absensi', 'Admin\AbsensiAnggotaController');
        // Route::get('/clear-cahce', function ()
        // {
        //     Artisan::call('cache:clear');
        // });

    });


});

require __DIR__.'/User.php';




