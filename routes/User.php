<?php

use App\Http\Controllers\PemimpinKelompok\AbsenController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
/**
 * User Area
 */
Route::group(['middleware' => ['role:user', 'auth']], function () {
    
    Route::name('user.')->prefix('/user')->group(function ()
    {
        Route::get('/index', function() {
            return '123';
        })->name('index');
        Route::resource('absen', 'PemimpinKelompok\AbsenController');
        Route::get('/clear-cahce', function ()
        {
            Artisan::call('cache:clear');
        });

    });


});