<?php

namespace Database\Seeders;

use App\Models\Anggotas;
use Illuminate\Database\Seeder;

class AnggotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nama = [
            'Iman' => 'A', 
            'Maulana' => 'B', 
            'Agus' => 'C', 
            'Adon' => 'A', 
            'Adi' => 'B', 
            'Zaki' => 'C', 
            'Yoga' => 'A', 
            'Budi' => 'B', 
            'Yudi' => 'C'
        ];
        for ($i=0; $i < 8; $i++) { 
            Anggotas::create([
                'nama_anggota' => array_keys($nama)[$i],
                'jabatan' => 'Anggota',
                'group_piket' => array_values($nama)[$i]
            ]);
        }
    }
}
