<?php

namespace Database\Seeders;

use App\Models\AnggotaPiket;
use Illuminate\Database\Seeder;

class TrxAnggotaPiketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dump_group_piket = [
            1 => 'A', 
            2 => 'B',
            3 => 'C', 
            4 => 'A', 
            5 => 'B', 
            6 => 'C', 
            7 => 'A',
            8 => 'B', 
            9 => 'C'
        ];
        for ($i=1; $i <= 9; $i++) { 
            AnggotaPiket::create([
                'anggota_id' => $i,
                'group_piket' => $dump_group_piket[$i],
            ]);
        }
    }
}
