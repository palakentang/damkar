<?php

namespace App\Respositories\AbsensiAnggota;

use App\Traits\PiketTraits;
use App\Models\AnggotaPiket;
use Illuminate\Support\Facades\DB;

class AbsensiAnggotaRepository implements AbsensiAnggotaInterface {
    use PiketTraits;
    public function showAll() 
    {
        return AnggotaPiket::with('Anggota')->latest();
    }
    /**
     * Create Data
     */
    public function create($data) {
        try {
            DB::beginTransaction();
            foreach ($data['group_piket'] as $key => $value) {
                $keterangan = isset($data['keterangan'][$key]) ?: null;
                AnggotaPiket::create([
                    'anggota_id' => $key,
                    'group_piket' => $value,
                    'keterangan' => $keterangan
                ]);
            }
            DB::commit();
            return ['success' => true];
        } catch (\Throwable $th) {
            DB::rollBack();
            return ['success' => true];
        }
    }

    /**
     * Show Data
     */
    public function find($id) {
        
    }

    /**
     * Update Data
     */
    public function update($id, $data) {

    }

    /**
     * Delete Data
     */
    public function delete($id) {

    }

    public function countingPresensiByDate($date) {
        $data = new \stdClass;
        $data->piket_hadir = AnggotaPiket::whereDate('created_at', $date)->where('group_piket', $this::$_PIKET_HADIR)->get()->count();
        $data->cadangan_piket = AnggotaPiket::whereDate('created_at', $date)->where('group_piket', $this::$_CADANGAN_PIKET)->get()->count();
        $data->lepas_tiket = AnggotaPiket::whereDate('created_at', $date)->where('group_piket', $this::$_LEPAS_PIKET)->get()->count();
        $data->tidak_hadir = AnggotaPiket::whereDate('created_at', $date)->where('group_piket', $this::$_TIDAK_HADIR)->get()->count();
        return $data;
        
    }
}