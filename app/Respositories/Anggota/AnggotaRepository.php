<?php

namespace App\Respositories\Anggota;

use App\Models\Anggotas;

class AnggotaRepository implements AnggotaInterface {

    /**
     * Show All Data
     */
    public function showAll() {
        return Anggotas::latest();
    }

    /**
     * Create Data
     */
    public function create($data) {
        $data = (object) $data;
    }

    /**
     * Show Data
     */
    public function find($id) {

    }

    /**
     * Update Data
     */
    public function update($id, $data) {

    }

    /**
     * Delete Data
     */
    public function delete($id) {

    }
}