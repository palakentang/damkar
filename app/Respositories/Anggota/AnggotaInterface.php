<?php
namespace App\Respositories\Anggota;

interface AnggotaInterface {
    public function showAll();
    public function create($data);
    public function find($id);
    public function update($id, $data);
    public function delete($id);
}