<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Models\Anggotas;
use App\Models\AnggotaPiket;

trait PiketTraits
{
    static $master_piket = [
        'A' => 'Piket Hadir',
        'B' => 'Cadangan Piket',
        'C' => 'Lepas Piket',
        'Z' => 'Tidak Hadir'
    ];
    
    protected static $_PIKET_HADIR = 'A';
    protected static $_CADANGAN_PIKET = 'B';
    protected static $_LEPAS_PIKET = 'C';
    protected static $_TIDAK_HADIR = 'Z';

    /**
     * Cek Apakah Hari ini sudah selesai Absen?
     * @return Boolean;
     */
    public static function isAlreadyAbsentToday($member_id = null)
    {
        if ($member_id === null) {
            $alreadyAbsentsToday = AnggotaPiket::whereDate('created_at', Carbon::today())->get()->count();
            if ($alreadyAbsentsToday === Anggotas::get()->count()) {
                return true;
            }else{
                return false;
            }
        }else{

            // Useless
            $alreadyAbsentToday = AnggotaPiket::whereDate('created_at', date('d-m-Y'))->where('anggota_id', $member_id)->get()->count();
            if ($alreadyAbsentToday > 0) {
                return true;
            }else{
                return false;
            }
        }
        
    }



}
