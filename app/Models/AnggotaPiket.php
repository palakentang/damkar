<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AnggotaPiket extends Model
{
    use HasFactory;
    protected $table = 'trx_anggota_piket';
    protected $guarded = [];
    // protected $appends = ['alasan'];

    // public function getAlasanAttribute() {
    //     return $this->keterangan ?: '-';
    // }

    /**
     * Get the Anggota associated with the AnggotaPiket
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Anggota(): HasOne
    {
        return $this->hasOne(Anggotas::class, 'id', 'anggota_id');
    }
    
}
