<?php

if(!function_exists('date_humans')) {
    function date_humans($date) {
        $month = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $explode = explode('-', $date);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $explode[0] . ' ' . $month[ (int)$explode[1] ] . ' ' . $explode[2];
    }
}