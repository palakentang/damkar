<?php

namespace App\Http\Controllers\PemimpinKelompok;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Respositories\AbsensiAnggota\AbsensiAnggotaRepository;
use App\Respositories\Anggota\AnggotaRepository;
use App\Traits\PiketTraits;

class AbsenController extends Controller
{
    use PiketTraits;
    
    protected $absensiAnggotaRepository, $anggotaRepository, $pageTitle;
    public function __construct(AbsensiAnggotaRepository $absensiAnggotaRepository, AnggotaRepository $anggotaRepository) {
        $this->absensiAnggotaRepository = $absensiAnggotaRepository;
        $this->anggotaRepository = $anggotaRepository;

        $this->pageTitle = 'Absensi Anggota';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this::isAlreadyAbsentToday()) {
            return view('errors.already_absent_today', [
                'pageTitle' => 'Maaf, hari ini sudah selesai Absen',
                'master_piket' => $this::$master_piket
            ]);    
        } 
        $data = $this->anggotaRepository->showAll();
        return view('User.pages.Absen.index', [
            'pageTitle' => 'Absen Anggota',
            'data' => $data->get(),
            'master_piket' => $this::$master_piket
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this::isAlreadyAbsentToday()) {
            return redirect('user.absen.index')->with('error', 'Gagal');
        }

        $create = $this->absensiAnggotaRepository->create($request->all());
        if ($create['success']) {
            return redirect()->route('user.absen.index')->with('success', 'Berhasil');
        }else{
            return redirect()->route('user.absen.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
