<?php

namespace App\Http\Controllers\Admin;

use App\Traits\PiketTraits;
use App\Models\AnggotaPiket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Respositories\AbsensiAnggota\AbsensiAnggotaRepository;

class AbsensiAnggotaController extends Controller
{
    use PiketTraits;
    protected $absensiAnggotaRepository, $pageTitle;
    public function __construct(AbsensiAnggotaRepository $absensiAnggotaRepository) {
        $this->absensiAnggotaRepository = $absensiAnggotaRepository;

        $this->pageTitle = 'Absensi Anggota';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
            $absensiAnggota = $this->absensiAnggotaRepository->showAll();
            
            if ($request->filled('from_date')) {
                $absensiAnggota = $absensiAnggota->whereDate('created_at', $request->from_date);
            }

            if ($request->filled('master_piket')) {
                $absensiAnggota = $absensiAnggota->where('group_piket', $request->master_piket)->whereDate('created_at', $request->from_date);
            }
  
            return DataTables::of($absensiAnggota)
           
            ->make();
        }
        return view('Admin.pages.AbsensiAnggota.index', [
            'pageTitle' => $this->pageTitle,
            'master_piket' => $this::$master_piket
        ]);
        // $data = $this->absensiAnggotaRepository->showAll();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($date)
    {
        return \Response::json($this->absensiAnggotaRepository->countingPresensiByDate($date));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
